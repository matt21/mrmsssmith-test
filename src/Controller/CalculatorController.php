<?php

namespace App\Controller;

use App\Entity\Calculation;
use App\Form\CalculationType;
use App\Service\CalculatorService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/calculator")
 *
 * Class CalculatorController
 * @package App\Controller
 */
class CalculatorController extends AbstractController
{
    /**
     * @Route("/", name="calculator_calculate")
     *
     * @param CalculatorService $calculator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calculate(CalculatorService $calculator, Request $request)
    {
        $form = $this->createForm(
            CalculationType::class,
            new Calculation()
        );

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $result = $calculator->calculate($form->getData());

            return $this->redirectToRoute('calculator_result', [
                'result' => $result->getResult()
            ]);
        }

        return $this->render('calculator/calculate.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/result", name="calculator_result")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function result(Request $request)
    {
        return $this->render('calculator/result.html.twig', [
            'result' => $request->get('result'),
        ]);
    }
}