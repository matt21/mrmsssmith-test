<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Calculation
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(
     *     "numeric",
     *     message="Provide a numeric value"
     * )
     */
    protected $firstValue;
        /**
         * @return mixed
         */
        public function getFirstValue()
        {
            return $this->firstValue;
        }

        /**
         * @param mixed $firstValue
         */
        public function setFirstValue($firstValue): void
        {
            $this->firstValue = $firstValue;
        }

    /**
     * @Assert\NotBlank()
     * @Assert\Type(
     *     "numeric",
     *     message="Provide a numeric value"
     * )
     */
    protected $secondValue;
        /**
         * @return mixed
         */
        public function getSecondValue()
        {
            return $this->secondValue;
        }

        /**
         * @param mixed $secondValue
         */
        public function setSecondValue($secondValue): void
        {
            $this->secondValue = $secondValue;
        }

    /**
     * @Assert\Choice(
     *     choices = { "add", "sub", "mul", "div" },
     *     message = "Choose a valid operation."
     * )
     */
    protected $operation;
        /**
         * @return mixed
         */
        public function getOperation()
        {
            return $this->operation;
        }

        /**
         * @param mixed $operation
         */
        public function setOperation($operation): void
        {
            $this->operation = $operation;
        }

    /**
     * @Assert\Type(
     *     "string",
     *     message="Result must be a string"
     * )
     */
    protected $result;
        /**
         * @return mixed
         */
        public function getResult()
        {
            return $this->result;
        }

        /**
         * @param mixed $result
         */
        public function setResult($result): void
        {
            $this->result = $result;
        }
}