<?php

namespace App\Service;

use App\Entity\Calculation;

class CalculatorService
{
    public function calculate(Calculation $calculation): Calculation
    {
        bcscale(4);
        $operation = 'bc'.$calculation->getOperation();
        $result = $operation(
            $calculation->getFirstValue(),
            $calculation->getSecondValue()
        );
        $calculation->setResult($result);
        return $calculation;
    }
}